const 
  fs = require('fs'),
  S3 = require('gulp-s3');
  glob = require('glob');

module.exports = function(s3options) {
  const s3 = new S3(s3options);

  async function clearBucket(bucket) {
    console.log('Clearing bucket');
    const objects = await s3.listObjects(bucket);
    if (objects) {
      const keys = objects.map(o => o.Key);
      if (keys.length) {
        await s3.deleteObjects(bucket, keys);
      }
    }
  }

  async function upload(path, bucket, removablePrefix) {
    const stat = await fs.promises.stat(path);
    if (stat.isFile()) {
      console.log(path);
      const key = path.replace(removablePrefix, '');
      await s3.uploadFile(path, bucket, key, null, true);
    }
    else {
      const entries = await fs.promises.readdir(path);
      for (let i = 0; i < entries.length; i++) {
        const entry = entries[i];
        await upload(`${path}/${entry}`, bucket, removablePrefix);
      } 
    }
  }

  this.deployPath = async (path, bucket, removablePrefix) => {
    await clearBucket(bucket);
    await upload(path, bucket, removablePrefix);
  };

  function getPaths(pattern, ignore) {
    return new Promise((res, rej) => {
      let options;
      if (ignore && ignore.length)
        options = { ignore };
      glob(pattern, options, (err, paths) => {
        if (err)
          rej(err);
        else
          res(paths);
      });
    });
  }
  
  this.deployGlob = async (pattern, ignore, bucket, removablePrefix) => {
    const paths = await getPaths(pattern, ignore);
    const count = paths.length;
    await clearBucket(bucket);
    for (let i = 0; i < count; i++) {
      const path = paths[i];
      const stat = await fs.promises.stat(path);
      if (stat.isFile()) {
        console.log(`${i + 1} / ${count}: ${path}`);
        await upload(path, bucket, removablePrefix);
      }
    }
  };
};